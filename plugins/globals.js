import Vue from 'vue'
import translations from '../translations/en'

export default ({ app, store }) => {
    Vue.prototype.$t = (key) => translations[key] || '';
}

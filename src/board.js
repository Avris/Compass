import translations from '../translations/en';

export class Board {
    constructor(size = 13, centreWidth = 3, questions = [], data = undefined) {
        this.size = size;
        this.centreWidth = centreWidth;
        this.questions = questions;
        this.setData(data);
    }

    setData(data) {
        if (data === undefined || data === false) {
            this.data = new Array(this.size * this.size).fill(false);
        } else if (data === true) {
            this.data = new Array(this.size * this.size).fill(true);
        } else if (Array.isArray(data)) {
            this.data = data.slice(0, this.size * this.size);
        } else {
            this.data = data.substring(0, this.size * this.size).split('').map(x => !!parseInt(x));
        }
    }

    toString() {
        return this.data.map(x => x ? '1' : '0').join('');
    }

    get(row, col) {
        return this.data[row * this.size + col];
    }

    set(row, col, value) {
        this.data[row * this.size + col] = value;
    }

    switch(row, col) {
        this.data[row * this.size + col] = !this.data[row * this.size + col];
    }

    getSection(coordinate) {
        const sideWidth = (this.size - this.centreWidth) / 2;
        if (coordinate < sideWidth) {
            return 0;
        }
        if (coordinate < sideWidth + this.centreWidth) {
            return 1;
        }
        return 2;
    }
}

export const board = new Board(13, 3, translations.questions);

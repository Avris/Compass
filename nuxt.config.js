import translations from './translations/en';

const banner = 'https://compass.avris.it/banner.png';
const colour = '#333333';

export default {
    target: 'static',
    head: {
        title: 'Compass',
        meta: [
            { charset: 'utf-8' },

            { hid: 'description', name: 'description', content: translations.description },

            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'apple-mobile-web-app-title', name: 'apple-mobile-web-app-title', content: translations.title },
            { hid: 'theme-color', name: 'theme-color', content: colour },

            { hid: 'og:type', property: 'og:type', content: 'article' },
            { hid: 'og:title', property: 'og:title', content: translations.title },
            { hid: 'og:description', property: 'og:description', content: translations.description },
            { hid: 'og:site_name', property: 'og:site_name', content: translations.title },
            { hid: 'og:image', property: 'og:image', content:  banner},

            { hid: 'twitter:card', property: 'twitter:card', content: 'summary_large_image' },
            { hid: 'twitter:title', property: 'twitter:title', content: translations.title },
            { hid: 'twitter:description', property: 'twitter:description', content: translations.description },
            { hid: 'twitter:site', property: 'twitter:site', content: process.env.BASE_URL },
            { hid: 'twitter:image', property: 'twitter:image', content: banner },
        ],
        link: [
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Dosis&display=swap'},
            { rel: 'shortcut icon', type: 'image/svg+xml', id: 'favicon', href: '/logo.svg' },
        ]
    },
    css: [],
    plugins: [
        { src: '~/plugins/globals.js' },
    ],
    components: true,
    buildModules: [],
    modules: [
        '@nuxtjs/pwa',
        'vue-plausible',
    ],
    pwa: {
        manifest: {
            name: translations.title,
            short_name: translations.title,
            description: translations.description,
            background_color: '#ffffff',
            theme_color: colour,
            lang: 'en',
        }
    },
    plausible: {
        domain: 'compass.avris.it',
        apiHost: 'https://plausible.avris.it',
    },
}
